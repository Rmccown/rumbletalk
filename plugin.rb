# name: rumbletalk
# version: 0.1.0
# about: A simple plugin to embed Rumbletalk in your site
# authors: Bob McCown

enabled_site_setting :rumbletalk_enabled

after_initialize do
  load File.expand_path('../app/controllers/rumbletalk_controller.rb', __FILE__)

  Discourse::Application.routes.append do
    get '/rumbletalk' => 'rumbletalk#index'
  end
end