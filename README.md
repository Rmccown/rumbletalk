# rumbletalk

Rumbletalk is a plugin to embed the Rumbletalk live chat into a Discourse site.

## Installation

### Install the plugin

Follow [Install a Plugin](https://meta.discourse.org/t/install-a-plugin/19157)
how-to from the official Discourse Meta, using `git clone https://github.com/rmccown/rumbletalk.git`
as the plugin command.

### Create an account and chatroom
Go to [Rumbletalk](https://rumbletalk.com) and create an account, then create a chatroom.  Copy your chatroom hash and ID from the embed code, and set them in Discourse plugin admin.

## Feedback

If you have issues or suggestions for the plugin, please bring them up on
[Discourse Meta](https://meta.discourse.org).

## TODO

* Some stuff, I'm sure...
