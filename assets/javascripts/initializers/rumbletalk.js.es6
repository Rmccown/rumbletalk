import { withPluginApi } from "discourse/lib/plugin-api";

export default {
  name: "rumbletalk",
  initialize(container) {
    withPluginApi("0.8.11", (api) => {
      const currentUser = api.getCurrentUser();
      const siteSettings = container.lookup("site-settings:main");
      const isNavLinkEnabled = siteSettings.rumbletalk_nav;
      if (isNavLinkEnabled) {
        api.addNavigationBarItem({
          name: "chatroom",
          displayName: 'Chatroom',
          href: "/rumbletalk",
        });
      }
    });
  },
};