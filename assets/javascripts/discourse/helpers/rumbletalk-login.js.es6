import User from 'discourse/models/user';
import { helperContext } from "discourse-common/lib/helpers";
import RSVP from "rsvp";

export function rumbletalkLogin(params) {
    var username = User.currentProp('name');
    var hash = helperContext().siteSettings.rumbletalk_hash;
    setTimeout(() => {
        const size = 240;
        const avatarTemplate = User.currentProp('avatar_template');
        const avatarHost = helperContext().siteSettings.rumbletalk_avatar_host;
        const avatarURL = avatarHost + avatarTemplate.replace(/\{size\}/, size);
        rtmq(
            'login',
            {
                hash: hash,
                username: username,
                image: avatarURL,
            }
        )    
    }, 1250);
}

export default Ember.Helper.helper(rumbletalkLogin);
