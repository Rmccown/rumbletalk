import User from 'discourse/models/user';
import { helperContext } from "discourse-common/lib/helpers";
import RSVP from "rsvp";

export function rumbletalkReset(params) {
    var hash = helperContext().siteSettings.rumbletalk_hash;
    try {
        delete window.RumbleTalkChat[hash]
    } catch(error) {
    }
}

export default Ember.Helper.helper(rumbletalkReset);
