import { createWidget } from 'discourse/widgets/widget';
import { h } from 'virtual-dom';

createWidget('rumbletalk-widget', {
  tagName: 'div.rumbletalk-embed',

  html() {
    const contents = [
      h("div", [
        h("div#" + this.siteSettings.rumbletalk_id, {
          attributes: { style: "height: 500px"},
        }),
        h("script", {
          attributes: { src: "https://rumbletalk.com/client/?" + this.siteSettings.rumbletalk_hash },
        }
        ),
      ])
    ];
    return contents;
  }
});
