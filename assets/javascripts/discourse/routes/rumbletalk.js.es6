import { helperContext } from "discourse-common/lib/helpers";

/**
 * Route for the path `/rumbletalk` as defined in `../rumbletalk-route-map.js.es6`.
 */
export default Ember.Route.extend({
  renderTemplate() {
    // Renders the template `../templates/rumbletalk.hbs`
    this.render('rumbletalk');
  }
});