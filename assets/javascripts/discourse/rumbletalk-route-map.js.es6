/**
 * Links the path `/rumbletalk` to a route named `rumbletalk`. Named like this, a
 * route with the same name needs to be created in the `routes` directory.
 */
export default function () {
  this.route('rumbletalk', { path: '/rumbletalk' });
}